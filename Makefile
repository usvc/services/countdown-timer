IMAGE_URL=usvc/countdown-timer
# runbook recipes

## run as a docker image
docker: docker_build
	docker run --publish 8080:8080 $(IMAGE_URL):latest

## build the docker image
docker_build:
	yarn build
	docker build --tag $(IMAGE_URL):latest .

## publish the image to docker hub
docker_publish: docker_build
	docker push $(IMAGE_URL):latest

## build the project into a static site
build:
	yarn build

## run the project in development mode
dev:
	yarn start
