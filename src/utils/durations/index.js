export const createSelectedTiming = (duration) => {
  let label = '';
  const minutes = toMinutes(duration);
  const seconds = toSecondsLeft(duration);
  if (minutes < 1 && minutes > -1) {
    label = `${seconds} seconds`;
  } else {
    if (minutes === 1) {
      label = `1 minute`;
    } else if (minutes > 1) {
      label = `${minutes} minutes`;
    }
    if (seconds > 0) {
      label += ` and ${seconds} second${seconds > 1 ? 's' : ''}`;
    }
  }
  return {
    inMs: duration,
    label: label.trim(),
  };
};

export const toMinutes = (durationInMilliseconds) => Math.floor(durationInMilliseconds / 1000 / 60);
export const toSecondsLeft = (durationInMilliseconds) => Math.floor(durationInMilliseconds / 1000) % 60;
