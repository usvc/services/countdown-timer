import * as actions from './actions';
import {createSelectedTiming} from '../../utils/durations';
import {
  CONTROLS_STATE,
  INITIAL_STATE,
  INITIAL_TIMINGS_STATE,
} from './state';

export default (state = INITIAL_STATE, action) => {
  const now = (new Date()).getTime();
  switch(action.type) {
    case actions.DECREMENT_TIME:
      if ((state.timings.end - now) > 0) {
        return {
          ...state,
        };
      } else {
        clearInterval(state.interval);
        return {
          ...state,
          controlsState: CONTROLS_STATE.COMPLETED,
          interval: null,
          timings: {
            start: 0,
          },
        };
      }
    case actions.SET_DURATION:
      return {
        ...state,
        selectedTiming: createSelectedTiming(action.value),
        controlsState: CONTROLS_STATE.STOPPED,
      };
    case actions.START_COUNTDOWN:
      const timeRemaining = (state.controlsState === CONTROLS_STATE.PAUSED) ?
        state.timings.left
        : state.selectedTiming.inMs;
      const endingTime = now + timeRemaining;
      return {
        ...state,
        controlsState: CONTROLS_STATE.STARTED,
        interval: action.value,
        timings: {
          start: now,
          pause: null,
          left: timeRemaining,
          end: endingTime,
        },
      };
    case actions.PAUSE_COUNTDOWN:
      clearInterval(state.interval);
      return {
        ...state,
        controlsState: CONTROLS_STATE.PAUSED,
        timings: {
          start: state.timings.start,
          pause: now,
          left: state.timings.left - (now - state.timings.start),
          end: null,
        },
      };
    case actions.STOP_COUNTDOWN:
      clearInterval(state.interval);
      return {
        ...state,
        controlsState: CONTROLS_STATE.STOPPED,
        timings: INITIAL_TIMINGS_STATE,
      };
    default:
      return state;
  }
};
