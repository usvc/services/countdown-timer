import * as actions from './actions';
import {createStore} from 'redux';
import {INITIAL_STATE} from './state';
import reducer from './reducers';
export {actions, reducer, INITIAL_STATE};

export const store = createStore(reducer, INITIAL_STATE);
