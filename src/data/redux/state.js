import {createSelectedTiming} from '../../utils/durations';

export const INITIAL_DURATION = createSelectedTiming(1000 * 60 * 5);

export const CONTROLS_STATE = {
  PAUSED: 'PAUSED',
  STARTED: 'STARTED',
  STOPPED: 'STOPPED',
  COMPLETED: 'COMPLETED',
};

export const INITIAL_CONTROLS_STATE = CONTROLS_STATE.STOPPED;

export const INITIAL_TIMINGS_STATE = {
  start: null,
  pause: null,
  left: null,
  end: null,
};

export const INITIAL_STATE = {
  selectedTiming: INITIAL_DURATION,
  controlsState: INITIAL_CONTROLS_STATE,
  interval: null,
  timings: INITIAL_TIMINGS_STATE,
};
