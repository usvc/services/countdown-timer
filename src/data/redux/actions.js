export const DECREMENT_TIME = 'DECREMENT_TIME';
export const decrementTime = () => ({
  type: DECREMENT_TIME,
});

export const SET_DURATION = 'SET_DURATION';
export const setDuration = (duration) => ({
  value: duration,
  type: SET_DURATION,
});

export const START_COUNTDOWN = 'START_COUNTDOWN';
export const startCountdown = (dispatch) => ({
  value: setInterval(() => {
    dispatch(decrementTime())
  }, 100),
  type: START_COUNTDOWN,
});

export const STOP_COUNTDOWN = 'STOP_COUNTDOWN';
export const stopCountdown = () => ({
  type: STOP_COUNTDOWN,
});

export const PAUSE_COUNTDOWN = 'PAUSE_COUNTDOWN';
export const pauseCountdown = () => ({
  type: PAUSE_COUNTDOWN,
});

export const mapDispatchToProps = (dispatch) => ({
  actions: {
    pauseCountdown: () => dispatch(pauseCountdown()),
    setDuration: (duration) => dispatch(setDuration(duration)),
    startCountdown: () => dispatch(startCountdown(dispatch)),
    stopCountdown: () => dispatch(stopCountdown()),
  },
});
