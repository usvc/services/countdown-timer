import React from 'react';
import {connect} from 'react-redux';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

import {
  CONTROLS_STATE,
} from '../../data/redux/state';
import {createSelectedTiming} from '../../utils/durations';
import {mapDispatchToProps} from '../../data/redux/actions';

const durations = [
  createSelectedTiming(5000),
  createSelectedTiming(30000),
  createSelectedTiming(60000),
  createSelectedTiming(60000 * 5),
  createSelectedTiming(60000 * 10),
  createSelectedTiming(60000 * 15),
  createSelectedTiming(60000 * 20),
  createSelectedTiming(60000 * 30),
  createSelectedTiming(60000 * 45),
  createSelectedTiming(60000 * 60),
];

export const Settings = connect(
  (state) => ({state}),
  mapDispatchToProps,
)(({state, actions}) => {
  return (
    <div className="settings">
      <TextField
        className="settings-selector"
        disabled={!(state.controlsState === CONTROLS_STATE.STOPPED || state.controlsState === CONTROLS_STATE.COMPLETED)}
        onChange={(event) => {
          actions.setDuration(event.target.value);
        }}
        value={state.selectedTiming.inMs}
        fullWidth
        select
        SelectProps={{
          MenuProps: {
            className: "settings-selector-menu-item",
          },
        }}
      >
        {durations.map(duration => (
          <MenuItem
            key={duration.inMs}
            value={duration.inMs}
          >
            {duration.label}
          </MenuItem>
        ))}
      </TextField>
    </div>
  );
});
