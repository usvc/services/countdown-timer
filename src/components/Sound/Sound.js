import React from 'react';
import {connect} from 'react-redux';

import {
  CONTROLS_STATE,
} from '../../data/redux/state';
import {mapDispatchToProps} from '../../data/redux/actions';

export const Sound = connect(
  (state) => ({state}),
  mapDispatchToProps,
)(({state, actions}) => {
  const playSound = state.controlsState === CONTROLS_STATE.COMPLETED;
  const player = playSound ? <video autoPlay loop src="/alarm.mp3" /> : '';
  return (
    <div className="sound">
      {player}
    </div>
  );
});
