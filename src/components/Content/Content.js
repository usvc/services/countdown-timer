import React from 'react';
import {connect} from 'react-redux';

import {
  CONTROLS_STATE,
} from '../../data/redux/state';
import {mapDispatchToProps} from '../../data/redux/actions';

export const Content = connect(
  (state) => ({state}),
  mapDispatchToProps,
)(({state, children}) => {
  const isCompleted = state.controlsState === CONTROLS_STATE.COMPLETED;
  return (
    <div className={`content-container ${isCompleted ? 'completed' : ''}`}>
      <div className="content">
        <div className="content-content">
          {children}
        </div>
      </div>
    </div>
  );
});
