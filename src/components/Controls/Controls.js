import React from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {
  CONTROLS_STATE,
} from '../../data/redux/state';
import {mapDispatchToProps} from '../../data/redux/actions';

export const Controls = connect(
  (state) => ({state}),
  mapDispatchToProps,
)(({state, actions}) => {
  const buttons = [
    {
      action: actions.stopCountdown,
      disabled: state.controlsState === CONTROLS_STATE.STOPPED,
      faIconString: (state.controlsState === CONTROLS_STATE.COMPLETED)
        ? 'sync' : 'stop',
      id: 'stop',
      label: 'stop the countdown',
    },
    {
      action: actions.startCountdown,
      disabled: state.controlsState === CONTROLS_STATE.STARTED,
      faIconString: 'play',
      id: 'start',
      label: 'start the countdown',
    },
    {
      action: actions.pauseCountdown,
      disabled: state.controlsState !== CONTROLS_STATE.STARTED,
      faIconString: 'pause',
      id: 'pause',
      label: 'pause the countdown',
    },
  ];

  return (
    <div className="controls">
      {buttons.map((button) => (
        <div className={button.id} key={button.id}>
          <Button
            aria-label={button.label}
            className="button"
            disabled={button.disabled}
            onClick={button.action}
            fullWidth
          >
            <FontAwesomeIcon icon={button.faIconString} />
          </Button>
        </div>
      ))}
    </div>
  );
});
