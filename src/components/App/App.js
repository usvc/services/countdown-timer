import React from 'react';
import {Provider} from 'react-redux';
import {store} from '../../data/redux';

import Content from '../Content';
import Controls from '../Controls';
import Display from '../Display';
import Settings from '../Settings';
import Sound from '../Sound';

export function App() {
  return (
    <Provider store={store}>
      <div className="app">
        <Content>
          <Sound />
          <div className="spacer" />
          <div className="display-container">
            <Display />
          </div>
          <div className="spacer" />
          <div className="settings-container">
            <Settings />
          </div>
          <div className="controls-container">
            <Controls />
          </div>
        </Content>
      </div>
    </Provider>
  );
};
