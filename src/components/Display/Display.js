import React from 'react';

import {connect} from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import {mapDispatchToProps} from '../../data/redux/actions';

import { CONTROLS_STATE } from '../../data/redux/state';

export const Display = connect(
  (state) => ({state}),
  mapDispatchToProps,
)(({state, actions}) => {
  const now = (new Date()).getTime();
  var timeLeft;
  switch (state.controlsState) {
    case CONTROLS_STATE.STARTED:
      timeLeft = Math.ceil((state.timings.end - now) / 1000);
      break;
    case CONTROLS_STATE.PAUSED:
      timeLeft = Math.ceil(state.timings.left / 1000);
      break;
    case CONTROLS_STATE.COMPLETED:
      timeLeft = 0;
      break;
    case CONTROLS_STATE.STOPPED:
    default:
      timeLeft = Math.ceil(state.selectedTiming.inMs / 1000);
      break;
  }
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;
  const minutesZeroPrefix = minutes < 10 ? '0' : ''
  const minutesDisplay = `${minutesZeroPrefix}${minutes}`;
  const secondsZeroPrefix = seconds < 10 ? '0' : ''
  const secondsDisplay = `${secondsZeroPrefix}${seconds}`;
  const displayText = `${minutesDisplay} minutes ${secondsDisplay} seconds`;

  return (
    <div
      aria-label={displayText}
      className="display"
    >
      <Grid container spacing={0}>
        <Grid item xs={12} sm={5} className="minutes">
          {minutesDisplay}
        </Grid>
        <Hidden xsDown>
          <Grid item sm={2} >
            :
          </Grid>
        </Hidden>
        <Grid item xs={12} sm={5} className="seconds">
          {secondsDisplay}
        </Grid>
        <Grid item xs={12}>
          <span className="warning">
            (your window seems a little short, things might look wonky)
          </span>
        </Grid>
      </Grid>
    </div>
  );
});
