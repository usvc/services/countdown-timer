# Countdown Timer



A simple countdown timer intended for use in timeboxed discussions.

Check out the site: [https://timer.usvc.dev](https://timer.usvc.dev).

# Runbook

## Starting in development

```sh
yarn start
```

## Building for static hosting

```sh
yarn build
```

## Build the Docker image

```sh
docker build -t usvc/countdown:latest .;
```

## Run as a Docker image

```sh
docker run -p 8080:8080 usvc/countdown:latest;
```

## Publishing the Docker image

```sh
docker push usvc/countdown:latest;
```

## Run in a Docker Compose

```yaml
services:
  # ...
  countdown:
    build:
      context: ./
      dockerfile: ./Dockerfile
    image: usvc/countdown:latest
    ports:
      - 8080:8080
```

## Configuring the CI/CD pipeline

| Key | Description | 
| --- | --- |
| `IMAGE_REGISTRY_URL` | URL of the Docker registry to push to |
| `IMAGE_REGISTRY_USERNAME` | Username of the Docker registry in use |
| `IMAGE_REGISTRY_PASSWORD` | Password for the user specified for the Docker registry |

# License

This project is licensed under the [MIT license](./LICENSE).
